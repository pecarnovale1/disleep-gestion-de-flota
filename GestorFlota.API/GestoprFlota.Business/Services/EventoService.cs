﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Interfaces.Repositories;
using GestorFlota.DataAccess;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GestorFlota.Business.Services
{
    public class EventoService : IEventoService
    {
        private readonly IEventoRepository _eventoRepository;

        public EventoService(IEventoRepository eventoRepository)
        {
            _eventoRepository = eventoRepository;
        }

        public Task<EventoDTO> Get(int id)
        {
            return _eventoRepository.Get(id);
        }

        public Task<PageResult<EventoDTO>> ToPage(Pagination<SearchEventoDTO> pageParams)
        {
            return _eventoRepository.ToPage(pageParams);
        }

        public Task<StatsDTO> GetStats()
        {
            return _eventoRepository.GetStats();
        }

        public Task<Evento> Save(SaveEventoDTO evento) 
        {
            return _eventoRepository.Save(evento);
        }

        public Task<int> Delete(int id)
        {
            return _eventoRepository.Delete(id);
        }
    }
}
