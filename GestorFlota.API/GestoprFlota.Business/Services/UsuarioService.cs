﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Interfaces.Repositories;
using GestorFlota.Business.Interfaces.Services;
using GestorFlota.Business.Models;
using System.Threading.Tasks;

namespace GestorFlota.Business.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _userRepository;

        public UsuarioService(IUsuarioRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<Usuario> Login(LoginUserDTO userDTO)
        {
            return _userRepository.Login(userDTO);
        }

        public Task<Usuario> GetById(string id)
        {
            return _userRepository.GetById(id);
        }

    }
}
