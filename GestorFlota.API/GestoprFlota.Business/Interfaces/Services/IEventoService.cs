﻿
using GestorFlota.Business.Entities;
using GestorFlota.DataAccess;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GestorFlota.Business.Services
{
    public interface IEventoService
    {
        Task<PageResult<EventoDTO>> ToPage(Pagination<SearchEventoDTO> pageParams);
        Task<EventoDTO> Get(int id);
        Task<StatsDTO> GetStats();
        Task<Evento> Save(SaveEventoDTO evento);
        Task<int> Delete(int id);
    }
}
