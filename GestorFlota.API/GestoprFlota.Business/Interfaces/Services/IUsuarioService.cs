﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Models;
using System.Threading.Tasks;

namespace GestorFlota.Business.Interfaces.Services
{
    public interface IUsuarioService
    {
        Task<Usuario> Login(LoginUserDTO userDTO);
        Task<Usuario> GetById(string id);
    }
}
