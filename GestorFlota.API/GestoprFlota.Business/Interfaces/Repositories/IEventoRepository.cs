﻿using System.Threading.Tasks;
using System.Collections.Generic;
using GestorFlota.Business.Entities;
using GestorFlota.DataAccess;

namespace GestorFlota.Business.Interfaces.Repositories
{
    public interface IEventoRepository
    {
        Task<Evento> Save(SaveEventoDTO evento);
        Task<PageResult<EventoDTO>> ToPage(Pagination<SearchEventoDTO> pageParams);
        Task<StatsDTO> GetStats();
        Task<EventoDTO> Get(int id);
        Task<int> Delete(int id);
    }
}
