﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GestorFlota.Business.Entities
{

    public class Evento
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "El campo descripcion es requerido")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "El campo Fecha es requerido")]
        public DateTime Fecha { get; set; }

        [Required(ErrorMessage = "El campo tipo de evento es requerido")]
        public int IdTipoEvento { get; set; }

        [Required(ErrorMessage = "El campo vehiculo es requerido")]
        public int IdVehiculo { get; set; }

        [Required(ErrorMessage = "El campo conductor es requerido")]
        public int IdConductor { get; set; }

    }
    /*VALIDACIONES*/

    //Required
    //StringLenght
    //Range
    //CreditCard
    //Compare
    //Phone
    //RegularExpresion
    //Url
    //BindRequired
}
