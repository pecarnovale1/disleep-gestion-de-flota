﻿namespace GestorFlota.Business.Entities
{
    public class TipoVehiculo
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
