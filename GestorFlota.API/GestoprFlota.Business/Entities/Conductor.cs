﻿namespace GestorFlota.Business.Entities
{
    public class Conductor
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int RolId { get; set; }
    }
}
