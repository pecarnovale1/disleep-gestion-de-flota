﻿namespace GestorFlota.Business.Entities
{
    public class VehiculoConductor
    {
        public int Id { get; set; }
        public int VehiculoId { get; set; }
        public int ConductorId { get; set; }
    }
}
