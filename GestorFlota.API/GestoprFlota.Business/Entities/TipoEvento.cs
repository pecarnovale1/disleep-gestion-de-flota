﻿namespace GestorFlota.Business.Entities
{
    public class TipoEvento
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
