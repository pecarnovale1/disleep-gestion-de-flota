﻿namespace GestorFlota.Business.Settings
{
    public class ConfigSettings
    {
        public string ConnString { get; set; }
        public string ResourcesDirectory { get; set; }
        public string Key { get; set; }
    }
}
