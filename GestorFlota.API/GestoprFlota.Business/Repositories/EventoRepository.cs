﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Interfaces.Repositories;
using GestorFlota.Business.Settings;
using GestorFlota.DataAccess;
using ImageMagick;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GestorFlota.Business.Repositories
{
    public class EventoRepository : IEventoRepository
    {
        private readonly ConfigSettings _configSettings;

        public EventoRepository(IOptions<ConfigSettings> configSettings)
        {
            _configSettings = configSettings.Value;
        }


        public async Task<Evento> Save(SaveEventoDTO evento)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("Descripcion", evento.Descripcion);
            parameters.Add("Fecha", evento.Fecha);
            parameters.Add("IdTipoEvento", evento.IdTipoEvento);
            parameters.Add("IdVehiculo", evento.IdVehiculo);
            parameters.Add("IdConductor", evento.IdConductor);

            var inserted = await SqlDataAccessAutoBuild.Instance.GetOne<Evento>(_configSettings.ConnString, "Evento_INS", parameters);

            SaveImage(evento.Image,inserted.Id);

            return inserted;


        }

        private async void SaveImage(string imageData,int? imageName)
        {

            string path = _configSettings.ResourcesDirectory;
            string file = path + "/" + imageName + ".jpg";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var files = Directory.GetFiles(path, imageName + ".jpg", SearchOption.AllDirectories);

            foreach (var filesh in files)
            {
                File.Delete(filesh);
            }


            using (var imageFile = new FileStream(file, FileMode.Create))
            {
                var base64Data = Regex.Match(imageData, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                var bytes = Convert.FromBase64String(base64Data);
                await imageFile.WriteAsync(bytes, 0, bytes.Length);
                imageFile.Flush();
            }

            using (MagickImage resizedImage = new MagickImage(file))
            {
                resizedImage.Resize(660, 0);
                resizedImage.Write(file);
            }
        }

        public async Task<PageResult<EventoDTO>> ToPage(Pagination<SearchEventoDTO> pageParams)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("Page", pageParams.Page);
            parameters.Add("PageSize", pageParams.PageSize);

            var count = (int)await GestorFlota.DataAccess.DataAccess.GetValue(_configSettings.ConnString, "Evento_COUNT", parameters);
            var paged = await SqlDataAccessAutoBuild.Instance.ToPage<EventoDTO>(_configSettings.ConnString, "Evento_QRY", pageParams.Page, (int)count, parameters);

            return paged;
        }

        public async Task<StatsDTO> GetStats()
        {
            var parameters = new Dictionary<string, object>();
            var paged = await SqlDataAccessAutoBuild.Instance.GetOne<StatsDTO>(_configSettings.ConnString, "Evento_Stats_Current_Month", parameters);

            return paged;
        }

        public async Task<EventoDTO> Get(int id)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("Id", id);

            var news = await SqlDataAccessAutoBuild.Instance.GetOne<EventoDTO>(_configSettings.ConnString, "Evento_QRY", parameters);

            return news;
        }

       
        public async Task<int> Delete(int id)
        {
            var parameters = new Dictionary<string, object>();

                parameters.Add("Id", id);

            var idr = (int)await SqlDataAccessAutoBuild.Instance.GetOne<int>(_configSettings.ConnString, "Evento_DEL", parameters);

            return idr;
        }

    }
}
