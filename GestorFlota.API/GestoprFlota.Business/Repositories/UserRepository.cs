﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Interfaces.Repositories;
using GestorFlota.Business.Models;
using GestorFlota.Business.Settings;
using GestorFlota.DataAccess;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GestorFlota.Business.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ConfigSettings _configSettings;

        public UsuarioRepository(IOptions<ConfigSettings> configSettings)
        {
            _configSettings = configSettings.Value;
        }

        public Task<Usuario> Login(LoginUserDTO userDTO)
        {
            var parameters = new Dictionary<string, object>();

                parameters.Add("Email", userDTO.Email);
                parameters.Add("Password", System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userDTO.Password)));

            var user = SqlDataAccessAutoBuild.Instance.GetOne<Usuario>(_configSettings.ConnString, "Usuario_LOGIN", parameters);

            return user;
        }

        public Task<Usuario> GetById(string id)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("Id", id);

            var user = SqlDataAccessAutoBuild.Instance.GetOne<Usuario>(_configSettings.ConnString, "Usuario_GET", parameters);

            return user;
        }
    }
}
