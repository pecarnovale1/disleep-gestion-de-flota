﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestorFlota.Business.Models
{
    public class UserTokenDTO
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
