﻿using GestorFlota.DataAccess;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
namespace GestorFlota.Business.Entities
{
    public class SearchEventoDTO
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }

        public int TipoEventoId { get; set; }

        public string ConuctorNombre { get; set; }

        public string ConuctorApellido { get; set; }

        public string ConuctorDNI { get; set; }

        public string VehiculoTipo { get; set; }

        public string VehiculoNombre { get; set; }
    }
}
