﻿namespace GestorFlota.Business.Entities
{
    public class TipoEventoDTO 
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
