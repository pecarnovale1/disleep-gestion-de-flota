﻿using GestorFlota.DataAccess;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
namespace GestorFlota.Business.Entities
{
    public class SaveEventoDTO
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public DateTime Fecha { get; set; }

        public string Description { get; set; }

        public int IdTipoEvento { get; set; }
        public int IdVehiculo { get; set; }
        public int IdConductor { get; set; }

        public string Image { get; set; }
    }
}
