﻿using GestorFlota.DataAccess;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
namespace GestorFlota.Business.Entities
{
    public class EventoDTO
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int TipoEventoId { get; set; }
        public string TipoEvento { get; set; }
        public int VehiculoId { get; set; }
        public string Patente { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int TipoVehiculoId { get; set; }
        public string TipoVehiculoDesc { get; set; }
        public int ConductorId { get; set; }
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int TotalEventos { get; set; }
    }
}
