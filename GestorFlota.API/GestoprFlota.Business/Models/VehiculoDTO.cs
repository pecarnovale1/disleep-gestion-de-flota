﻿namespace GestorFlota.Business.Entities
{
    public class VehiculoDTO 
    {
        public int Id { get; set; }
        public string Patente { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int TipoVehiculoId { get; set; }
        public string TipoVehiculoDesc { get; set; }
    }
}
