﻿namespace GestorFlota.Business.Entities
{
    public class StatsDTO
    {
        public int IndiciosMicro { get; set; }
        public int PrincipiosMicro { get; set; }
        public int Micro { get; set; }
        public int Desvios { get; set; }
    }
}
