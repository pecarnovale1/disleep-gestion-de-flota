﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace GestorFlota.DataAccess
{
    public class SqlDataAccessCustomBuild : DataAccess
    {

        #region Singleton


        private static readonly SqlDataAccessCustomBuild instance = new SqlDataAccessCustomBuild();

        public static SqlDataAccessCustomBuild Instance
        {
            get { return instance; }
        }

        #endregion

        public async Task<List<T>> GetList<T>(string connectionKey, string procedureName, Dictionary<string, object> listParams) where T : IEntity
        {
            List<T> mappedList = new List<T>();

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    var reader = await cmd.ExecuteReaderAsync();

                    while (await reader.ReadAsync())
                    {
                        var obj = Activator.CreateInstance<T>();
                        mappedList.Add((T)obj.Build(reader));
                    }

                }
            }

            return mappedList;

        }

        public async Task<PageResult<T>> ToPage<T>(string connectionKey, string procedureName, int page, int totalRows, Dictionary<string, object> listParams) where T : IEntity
        {
            List<T> mappedList = new List<T>();

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    var reader = await cmd.ExecuteReaderAsync();

                    while (await reader.ReadAsync())
                    {
                        var obj = Activator.CreateInstance<T>();
                        mappedList.Add((T)obj.Build(reader));
                    }

                }
            }

            PageResult<T> paged = new PageResult<T>();

            paged.TotalRows = totalRows;
            paged.Page = page;
            paged.Result = mappedList;

            return paged;
        }

        public async Task<T> GetOne<T>(string connectionKey, string procedureName, Dictionary<string, object> listParams) where T : IEntity
        {
            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    var reader = await cmd.ExecuteReaderAsync();

                    while (reader.Read())
                    {
                        var obj = Activator.CreateInstance<T>();
                        return (T)obj.Build(reader);
                    }

                }

            }

            return Activator.CreateInstance<T>();
        }




    }
}
