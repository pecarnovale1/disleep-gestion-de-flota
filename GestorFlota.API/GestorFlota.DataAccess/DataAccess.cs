﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Reflection;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace GestorFlota.DataAccess
{
    public abstract class DataAccess
    {
        public static async Task<SqlConnection> NewConnection(string connectionKey)
        {
            var appSettingsConn = ConfigurationManager.AppSettings[connectionKey];
            var connectionsConn = ConfigurationManager.ConnectionStrings[connectionKey];

            var connString = appSettingsConn ?? (connectionsConn != null ? connectionsConn.ConnectionString : connectionKey);
            if (string.IsNullOrEmpty(connString))
                throw new Exception(string.Format("The connection string is empty. " +
                     "Please add an entry in the web.config file with the key '{0}' containing the connection string. " +
                     "Or set the connection string on 'connectionKey' parameter"
                    , connectionKey));
            try
            {
                var con = new SqlConnectionStringBuilder(connString);
            }
            catch
            {
                throw new Exception(string.Format("The connection string '{0}' is invalid", connectionKey));
            }

            var conn = new SqlConnection(connString);
            
            await conn.OpenAsync();

            return conn;
        }

        private static async Task<object> _getValue(string connectionKey, string procedureName, Dictionary<string, object> listParams, CommandType cmdType)
        {
            object returnedObj;

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {

                    cmd.CommandType = cmdType;
                    cmd.CommandTimeout = 0;

                    SetParams(cmd, listParams);

                    returnedObj = await cmd.ExecuteScalarAsync();

                }
            }

            return returnedObj;
        }

        public static async Task<object> GetValue(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            return await _getValue(connectionKey, procedureName, listParams, CommandType.StoredProcedure);
        }

        public static async Task<object> GetValueFromQuery(string connectionKey, string query, Dictionary<string, object> listParams)
        {
            return await _getValue(connectionKey, query, listParams, CommandType.Text);
        }

        public async void Execute(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    SetParams(cmd, listParams);

                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        internal static void SetParams(SqlCommand cmd, Dictionary<string, object> listParams)
        {
            cmd.CommandTimeout = 0;
            foreach (var param in listParams)
            {
                cmd.Parameters.Add(new SqlParameter(param.Key, param.Value));

            }
        }
    }


    public static class DataAccessHelper
    {
        public  static async Task<List<T>> DataReaderMapToList<T>(SqlDataReader dr)
        {
            var list = new List<T>();
            T obj = default(T);

            var columNames = new List<string>();

            for (int i = 0; i < dr.FieldCount; i++)
            {
                columNames.Add(dr.GetName(i));
            }


            while (await dr.ReadAsync())
            {
                obj = Activator.CreateInstance<T>();

                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (columNames.Contains(prop.Name) && !object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj,dr[prop.Name], null);
                    }
                }

                list.Add(obj);
            }
            return list;
        }

        public static async Task<T> DataReaderMapToObject<T>(SqlDataReader dr)
        {
            T obj = default(T);

            var columNames = new List<string>();

            for (int i = 0; i < dr.FieldCount; i++)
            {
                columNames.Add(dr.GetName(i));
            }

            if (await dr.ReadAsync())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (columNames.Contains(prop.Name) && !object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
            }
            return obj;
        }

        public static async Task<List<Dictionary<string, object>>> DataReaderMapToGenericList(SqlDataReader dr)
        {
            var columNames = new List<string>();
            var values = new List<Dictionary<string, object>>();

            for (int i = 0; i < dr.FieldCount; i++)
            {
                columNames.Add(dr.GetName(i));
            }

            while (await dr.ReadAsync())
            {
                var row = new Dictionary<string, object>();
                foreach (var column in columNames)
                {
                    var value = dr[column] != DBNull.Value ? dr[column] : null;
                    row.Add(column, value);
                }

                values.Add(row);
            }

            return values;
        }

        public static DataTable ConvertToDatatable<T>(IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var value = props[i].GetValue(item);
                    if (value != null)
                        values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static Dictionary<string, T> ToDictionary<T>(this object source)
        {

            var dictionary = new Dictionary<string, T>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(source))
            {
                object value = property.GetValue(source);
                if (IsOfType<T>(value))
                {
                    dictionary.Add(property.Name, (T)value);
                }
            }
            return dictionary;
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }
    }
}
