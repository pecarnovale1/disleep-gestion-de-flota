﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace GestorFlota.DataAccess
{
    public class SqlDataAccessArrayBuild : DataAccess
    {

        #region Singleton


        private static readonly SqlDataAccessArrayBuild instance = new SqlDataAccessArrayBuild();

        public static SqlDataAccessArrayBuild Instance
        {
            get { return instance; }
        }

        #endregion

        public async Task<IList> GetList(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            IList listFinal = new List<ArrayList>();

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var listElement = new ArrayList();


                            for (var a = 0; a <= reader.FieldCount - 1; a++)
                            {
                                listElement.Add(reader[a]);
                            }

                            listFinal.Add(listElement);
                        }


                        reader.Close();
                    }
                }
            }

            return listFinal;

        }


        public async Task<IList> GetOne(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            IList listFinal = new List<ArrayList>();

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {

                        while (await reader.ReadAsync())
                        {
                            var listElement = new ArrayList();


                            for (var a = 0; a <= reader.FieldCount - 1; a++)
                            {
                                listElement.Add(reader[a]);
                            }

                            listFinal.Add(listElement);
                        }


                        reader.Close();
                    }

                }

            }


            return listFinal;
        }

        public async Task<IList> GetGenericList(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            IList listFinal = new List<Dictionary<string, object>>();

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {

                        listFinal = await DataAccessHelper.DataReaderMapToGenericList(reader);

                        reader.Close();
                    }
                }
            }

            return listFinal;

        }




    }
}
