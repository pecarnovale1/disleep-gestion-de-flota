﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace GestorFlota.DataAccess
{
    public class SqlDataAccessAutoBuild : DataAccess
    {

        #region Singleton


        private static readonly SqlDataAccessAutoBuild instance = new SqlDataAccessAutoBuild();

        public static SqlDataAccessAutoBuild Instance
        {
            get { return instance; }
        }

        #endregion

        private async Task<List<T>> _getList<T>(string connectionKey, string query, Dictionary<string, object> listParams, CommandType cmdType)
        {
            List<T> mappedList;

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    cmd.CommandType = cmdType;

                    SetParams(cmd, listParams);

                    var reader = await cmd.ExecuteReaderAsync();

                    mappedList = await DataAccessHelper.DataReaderMapToList<T>(reader);


                }
            }

            return mappedList;

        }

        public async Task<PageResult<T>> ToPage<T>(string connectionKey, string procedureName, int page, int totalRows, Dictionary<string, object> listParams)
        {
            var result = await _getList<T>(connectionKey, procedureName, listParams, CommandType.StoredProcedure);

            PageResult <T> paged = new PageResult<T>();

            paged.TotalRows = totalRows;
            paged.Page = page;
            paged.Result = result;

            return paged;
        }

        public async Task<List<T>> GetList<T>(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            return await _getList<T>(connectionKey, procedureName, listParams, CommandType.StoredProcedure);

        }

        public async Task<List<T>> GetListFromQuery<T>(string connectionKey, string query, Dictionary<string, object> listParams)
        {
            return await _getList<T>(connectionKey, query, listParams, CommandType.Text);
        }

        public async void ExcecuteNonScalar(string connectionKey, string query, Dictionary<string, object> listParams)
        {
            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(query, conn))
                {
                    foreach (var param in listParams)
                    {
                        cmd.Parameters.Add(new SqlParameter(param.Key, param.Value));
                    }

                    try
                    {
                        await cmd.ExecuteNonQueryAsync();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                }
            }
        }


        public async Task<T> GetOne<T>(string connectionKey, string procedureName, Dictionary<string, object> listParams)
        {
            T mappedObj;

            using (var conn = await NewConnection(connectionKey))
            {
                using (var cmd = new SqlCommand(procedureName, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    SetParams(cmd, listParams);

                    var reader = await cmd.ExecuteReaderAsync();

                    mappedObj = await DataAccessHelper.DataReaderMapToObject<T>(reader);

                }

            }

            return mappedObj;
        }



    }
}
