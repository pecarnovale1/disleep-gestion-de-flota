﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace GestorFlota.DataAccess
{
    public interface IEntity
    {
        object Build(SqlDataReader reader);

    }
}
