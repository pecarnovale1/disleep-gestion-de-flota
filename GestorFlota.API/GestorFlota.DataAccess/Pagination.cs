﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestorFlota.DataAccess
{
    public class Pagination<T>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string Sort { get; set; }
        public T Filter { get; set; }
    }

    public class PageResult<T>
    {
        public int Page { get; set; }
        public int TotalRows { get; set; }
        public List<T> Result { get; set; }
    }
}
