﻿using GestorFlota.Business.Entities;
using GestorFlota.Business.Services;
using GestorFlota.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestorFlota.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventoController : ControllerBase
    {
        private readonly IEventoService _eventosService;

        public EventoController(IEventoService eventosService)
        {
            _eventosService = eventosService;
        }

        [AllowAnonymous]
        [HttpGet] 
        public Task<EventoDTO> Get(int id)
        {
            return _eventosService.Get(id);
        }

        [HttpPost("ToPage")]
        public Task<PageResult<EventoDTO>> ToPage(Pagination<SearchEventoDTO> pageParams)
        {
            return _eventosService.ToPage(pageParams);
        }

        [HttpGet("Stats")]
        public Task<StatsDTO> GetStats()
        {
            return _eventosService.GetStats();
        }

        [HttpPost]
        public Task<Evento> Save(SaveEventoDTO evento)
        {
            return _eventosService.Save(evento);
        }

        [HttpDelete]
        public Task<int> Delete(int id)
        {
            return _eventosService.Delete(id);
        }
    }
}
