﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using System.Text;
using GestorFlota.Business.Settings;
using GestorFlota.Business.Entities;
using GestorFlota.Business.Models;
using GestorFlota.Business.Interfaces.Services;
using System.Threading.Tasks;

namespace GestorFlota.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ConfigSettings _configSettings;
        private readonly IUsuarioService _userService;

        public UserController(IOptions<ConfigSettings> configSettings, IUsuarioService userService)
        {
            _configSettings = configSettings.Value;
            _userService = userService;
        }

        [HttpPost("Login")]
        public async Task<ActionResult<UserTokenDTO>> Login([FromBody] LoginUserDTO userInfo)
        {
            var result = await _userService.Login(userInfo);
            if (result != null)
            {
                return BuildToken(userInfo, result.Id, result.RolDescripcion);
            }
            else
            {
                ModelState.AddModelError("message", "Usuario o Password incorrecto");
                return BadRequest(ModelState);
            }
        }

        
        [Authorize]
        [HttpGet("CurrentAuthUser")]
        public async Task<ActionResult<Usuario>> CurrentAuthUser()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;

            var id = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

            var result = await _userService.GetById(id);

            return result;
        }

        private UserTokenDTO BuildToken(LoginUserDTO user,int id, string role)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            
            claims.Add(new Claim(ClaimTypes.Role, role));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, id.ToString()));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configSettings.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // Tiempo de expiración del token. En nuestro caso lo hacemos de una hora.
            var expiration = DateTime.UtcNow.AddMinutes(200);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: null,
               audience: null,
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return new UserTokenDTO()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
    }
}
