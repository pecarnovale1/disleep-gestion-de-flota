import React, { useContext, useReducer } from 'react';
import authToken from '../../api/AuthToken';
import axiosClient from '../../api/AxiosClient';
import { CLEAR_MSG, LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT, SET_USER } from './AuthTypes';
import AuthContext from './AuthContext';
import AuthReducer from './AuthReducer';
import moment from 'moment';

const AuthState = (props) => {


    //Esta autenticado y el token no expiro
    const isAuthenticated = () => {
        return localStorage.getItem('token') && (moment(localStorage.getItem('expiration')).diff(moment())>= 0)
    }

    //Si esta autenticado agrego token a peticion, sino expiro y tengo que volver a iniciar sesion
    if(isAuthenticated()) {
        authToken(localStorage.getItem('token'));
    } 


    const initialState = {
        isLoginLoading:false,
        token: localStorage.getItem('token'),
        expiration: localStorage.getItem('expiration'),
        isAuthenticated:isAuthenticated(),
        user:JSON.parse(localStorage.getItem('user')),
        msg:null
    }

    const [state,dispatch] = useReducer(AuthReducer,initialState);

    //Login Action
    const login = async (user) => {
        
        try {

            const response = await axiosClient.post('user/login',user);

            authToken(response.data.token);

            const appuser = await axiosClient.get('user/currentauthuser');

            dispatch({
                type:SET_USER,
                payload: appuser.data
            });
            
            dispatch({
                type:LOGIN_SUCCESS,
                payload: {
                    token:response.data.token,
                    expiration: response.data.expiration
                }

            });

        } catch (error) {
            if(error.response) {
                dispatch({
                    type:LOGIN_ERROR,
                    payload: error.response.data.message
                });
            } else {
                dispatch({
                    type:LOGIN_ERROR,
                    payload: "Error de red"
                });
            }
                
        }
    }


    //Logout action
    const logout = () => {
        dispatch({type:LOGOUT});
    }

    //ClearMsg action
    const clearMsg = () => {
        dispatch({type:CLEAR_MSG});
    }


    
    

    return  <AuthContext.Provider
                value={{
                    token:state.token,
                    isAuthenticated:state.isAuthenticated,
                    user:state.user,
                    msg:state.msg,
                    login,
                    logout,
                    clearMsg
                }}
            >
                {props.children}
            </AuthContext.Provider>
};

export default AuthState;