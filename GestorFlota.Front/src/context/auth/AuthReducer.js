import { 
    LOGIN_ERROR, 
    LOGIN_SUCCESS,
    LOGIN_REQUEST,
    LOGOUT,
    SET_USER,
    CLEAR_MSG
} from "./AuthTypes";

export default (state,action) => {
    switch(action.type) {

        case LOGIN_REQUEST :
            return {
                ...state,
                isLoginLoading:true
            };

        case LOGIN_ERROR :
            return {
                ...state,
                msg: action.payload
            };
        case CLEAR_MSG :
            return {
                ...state,
                msg: null
            };

        case LOGIN_SUCCESS :

            localStorage.setItem('token',action.payload.token);
            localStorage.setItem('expiration',action.payload.expiration);

            return {
                ...state,
                isAuthenticated: true,
                token:action.payload.token,
                expiration:action.payload.expiration,
                msg:null
            };

        case LOGOUT :
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
            localStorage.removeItem('user');

            return {
                ...state,
                isAuthenticated:false,
                token: null,
                expiration: null
            };

        case SET_USER :
            localStorage.setItem('user',JSON.stringify(action.payload));

            return {
                ...state,
                user:  action.payload
            };
        default:
            return state;
    }
}