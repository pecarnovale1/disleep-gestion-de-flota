export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const SET_USER = 'SET_USER';
export const LOGOUT = 'LOGOUT';
export const CLEAR_MSG = 'CLEAR_MSG';