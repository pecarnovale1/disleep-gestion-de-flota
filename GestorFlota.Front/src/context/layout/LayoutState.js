
import React, { useState } from 'react';
import LayoutContext from './LayoutContext';


const LayoutState = (props) => {

    const initialState = {
        showSidebar: true
    };    

    const [state,setState] = useState(initialState);

    const toggleSidebar = (value) => {
       setState({
           ...state,
           showSidebar: value
       })
    }


    return (
        <LayoutContext.Provider value={{
            showSidebar: state.showSidebar,
            toggleSidebar
        }}>
            {props.children}
        </LayoutContext.Provider>
    );

}


export default LayoutState;