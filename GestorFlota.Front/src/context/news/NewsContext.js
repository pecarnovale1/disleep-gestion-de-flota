const { createContext } = require("react");

const NewsContext = createContext();

export default NewsContext;