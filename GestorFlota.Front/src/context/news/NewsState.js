const { useState } = require("react")
const { default: NewsContext } = require("./NewsContext")


const NewsState = (props) => {

    const initialState = {
        news:[],
        loading:false,
        errors:''
    }

    const [state,setState] = useState(initialState);

    const getNews = () => {
        setState({});
    }

    return (
        <NewsContext.Provider value={{
            news:state.news,
            loading:state.loading,
            errors:state.errors,
            getNews
        }}>
            {props.children}
        </NewsContext.Provider>
    )
}

export default NewsState;