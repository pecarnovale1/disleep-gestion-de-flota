import React, {  useReducer } from 'react';
import Swal from 'sweetalert2';
import { GET_EVENTOS_TO_PAGE_REQUEST, GET_EVENTOS_TO_PAGE_SUCCESS, GET_EVENTO_REQUEST, GET_EVENTO_SUCCESS, GET_STATS_REQUEST,GET_STATS_SUCCESS, UNSELECT_EVENTO, UNSELECT_EVENTO_REQUEST} from './EventosTypes';
import EventosContext from './EventosContext';
import EventosReducer from './EventosReducer';
import { GetEvento, GetEventosToPage, GetStats } from 'src/api/services/EventosServices';

const EventosState = (props) => {

    const initialState = {
        events:[],
        eventSelected:null,
        totalEvents:0,
        loading:false,
        isStatsLoading:false,
        stats: null
    }

    const [state,dispatch] = useReducer(EventosReducer,initialState);

    //Get Stats
    const getStats = async () => {
      
        dispatch({
            type:GET_STATS_REQUEST
        });

       await GetStats().then((response) => {

            dispatch({
                type:GET_STATS_SUCCESS,
                payload: response
            });
            
        },
        (error) => {
            Swal.fire('Error','','error');
        })
    };


    const getEventosToPage = async (page,pagesize,filter) => {
      
        dispatch({
            type:GET_EVENTOS_TO_PAGE_REQUEST
        });

       await GetEventosToPage(page,pagesize,filter).then((response) => {

            dispatch({
                type:GET_EVENTOS_TO_PAGE_SUCCESS,
                payload: {
                    events:response.result,
                    totalEvents:response.totalRows
                }
            });
            
        },
        (error) => {
            Swal.fire('Error',error.response.data.message,'error');
        })
    };

    const getEvento = async (id) => {
      
        dispatch({
            type:GET_EVENTO_REQUEST
        });

       await GetEvento(id).then((response) => {

            dispatch({
                type:GET_EVENTO_SUCCESS,
                payload: response
            });
            
        },
        (error) => {
            Swal.fire('Error',error.response.data.message,'error');
        })
    };

    const unselectEvento = async () => {
      
        dispatch({
            type:UNSELECT_EVENTO_REQUEST
        });
    };
     

    return  <EventosContext.Provider
                value={{
                    events:state.events,
                    eventSelected:state.eventSelected,
                    totalEvents:state.totalEvents,
                    loading:state.loading,
                    isStatsLoading: state.isStatsLoading,
                    stats: state.stats,
                    getStats,
                    getEventosToPage,
                    getEvento,
                    unselectEvento
                }}
            >
                {props.children}
            </EventosContext.Provider>
};

export default EventosState;