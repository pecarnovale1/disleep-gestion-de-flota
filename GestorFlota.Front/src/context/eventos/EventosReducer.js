import { GET_EVENTOS_TO_PAGE_REQUEST, GET_EVENTOS_TO_PAGE_SUCCESS, GET_EVENTO_REQUEST, GET_EVENTO_SUCCESS, GET_STATS_REQUEST, GET_STATS_SUCCESS, UNSELECT_EVENTO_REQUEST } from "./EventosTypes";

export default (state,action) => {
    switch(action.type) {

        case UNSELECT_EVENTO_REQUEST :
            return {
                ...state,
                eventSelected:null
            };

        case GET_EVENTO_REQUEST :
            return {
                ...state,
                loading:true
            };

        case GET_EVENTO_SUCCESS :
            return {
                ...state,
                loading:false,
                eventSelected:action.payload
            };
        
        case GET_EVENTOS_TO_PAGE_REQUEST :
            return {
                ...state,
                loading:true
            };

        case GET_EVENTOS_TO_PAGE_SUCCESS :
            return {
                ...state,
                loading:false,
                events: action.payload.events,
                totalEvents: action.payload.totalEvents
            };

        case GET_STATS_REQUEST :
            return {
                ...state,
                isStatsLoading:true
            };
        case GET_STATS_SUCCESS :
            return {
                ...state,
                stats: action.payload,
                isStatsLoading:false
            };
        default:
            return state;
    }
}