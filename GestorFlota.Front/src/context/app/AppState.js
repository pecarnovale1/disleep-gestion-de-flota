import { useState } from "react";
import AppContext from "./AppContext";


const AppState = (props) => {


    const initialState = {
        error:{
            visible:false
        }
    }

    const [state,setState] = useState(initialState);

    const showError = (message,category) => {
        setState({
            ...state,
            error:{
                visible:true,
                message:message,
                category:category
            }
        })
    }

    const hideError = () => {
        setState({
            ...state,
            error:{
                visible:false
            }
        })
    }



    return <AppContext.Provider value={{
        error:state.error,
        showError,
        hideError
    }}>
        {props.children}
    </AppContext.Provider>

}

export default AppState;