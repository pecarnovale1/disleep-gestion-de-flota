import { CAlert } from '@coreui/react'
import React from 'react'
import LayoutState from 'src/context/layout/LayoutState'
import {
  Content,
  Sidebar,
  Footer,
  Header
} from './index'

const Layout = () => {


  return (
    <LayoutState>
      <div className="c-app c-default-layout">
        <Sidebar/>
        <div className="c-wrapper">
          <Header />
          <div className="c-body">
            <Content />
          </div>
          <Footer />
        </div>
      </div>
    </LayoutState>
  )
}

export default Layout
