import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Estadisticas',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    badge: {
      color: 'info',
      text: 'NUEVO',
    }
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Gestion de Flota de Transportes']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Eventos',
    route: '',
    icon: 'cil-newspaper',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Listado',
        to: '/eventos',
        icon: 'cil-plus'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Destacados',
        to: '/base/cards',
        icon: 'cil-check'
      }
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Conductores',
    route: '',
    icon: 'cil-paper-plane',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Nuevo',
        to: '/base/breadcrumbs',
        icon: 'cil-plus'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Listado',
        to: '/base/cards',
        icon: 'cil-list-rich'
      }
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Vehiculos',
    route: '',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Nuevo',
        to: '/base/breadcrumbs',
        icon: 'cil-plus'
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Listado',
        to: '/base/cards',
        icon: 'cil-list-rich'
      }
    ],
  },
  
]

export default _nav
