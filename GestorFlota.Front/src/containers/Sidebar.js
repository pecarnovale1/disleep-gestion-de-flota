import React, { useContext } from 'react'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'

// sidebar nav config
import navigation from './_nav'
import LayoutContext from 'src/context/layout/LayoutContext'

const Sidebar = () => {

  const {toggleSidebar,showSidebar} = useContext(LayoutContext);

  return (
    <CSidebar
      show={showSidebar}
      onShowChange={() => toggleSidebar(!showSidebar) }
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={130}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={130}
        />
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(Sidebar)
