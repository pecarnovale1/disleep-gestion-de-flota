import React, { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import {
  CAlert,
  CButton,
  CButtonClose,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import AuthContext from 'src/context/auth/AuthContext'
import { useForm } from "react-hook-form";
import { GlobalNotifications } from 'src/common'
import Alert from 'src/common/Alert'



const Login = (props) => {

  const {login, isAuthenticated,msg, clearMsg} = useContext(AuthContext);
  const { register,formState: { errors }, handleSubmit } = useForm();
 

  const onSubmit = data => {
    login(data);
  }  


  useEffect(() => {
    if(isAuthenticated)
      props.history.push('dashboard');
  }, [isAuthenticated]);

  return (
    
    <div className="c-app c-default-layout flex-row align-items-center">
      {!isAuthenticated ? 
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <form noValidate onSubmit={handleSubmit(onSubmit)}>
                    <h1>Login</h1>
                    <p className="text-muted">Identificarse en el sistema</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>

                      <input 
                         className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                        type="email" 
                        placeholder="Email" 
                        autoComplete="email" 
                        {...register("email", { 
                            required: "Email requerido",  
                            pattern: {
                              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                              message: 'Email incorrecto'
                            }
                          })
                        } 
                      />

                      {errors.email && errors.email.message && 
                        <div className="invalid-feedback">
                          {errors.email.message}
                        </div>
                      }

                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>

                      <input 
                        className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                        type="password" 
                        placeholder="Password" 
                        autoComplete="password" 
                        {...register("password", { 
                          required:   {value:true,message:"Password Requerido"},
                          minLength:  {value:5,message: "La longitud minima del password es de 5 caracteres"}, 
                          maxLength:  {value:10,message: "La longitud maxima del password es de 5 caracteres"} 
                        })} 
                      />

                      {errors.password && errors.password.message && 
                        <div className="invalid-feedback">
                          {errors.password.message}
                        </div>
                      }

                    </CInputGroup>
                    <CRow>
                      <CCol>
                        {msg && <CAlert  color="danger"  position="static">
                            {msg}
                            <CButtonClose  buttonClass="text-danger close" onClick={clearMsg}/>
                        </CAlert>
                        }
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" type="submit" className="px-4">Ingreso</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">¿Perdio su contraseña?</CButton>
                      </CCol>
                    </CRow>
                  </form>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Disleep</h2>
                    <p>Gestion de Flota</p>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
        <CRow className="justify-content-center">
          <CCol md="8" mt="2">
            <GlobalNotifications />
          </CCol>
        </CRow>
      </CContainer>
      :
      <div>Cargando</div>
      }
    </div>
  )
}

export default Login
