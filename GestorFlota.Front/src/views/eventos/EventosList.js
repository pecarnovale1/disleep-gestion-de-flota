import { CBadge, CCard, CCardBody, CCardHeader, CDataTable, CElementCover, CPagination } from '@coreui/react'
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react'
import EventosContext from 'src/context/eventos/EventosContext';

const EventosList = (props) => {

    const {getEventosToPage,events,totalEvents, loading:isEventsLoading} = useContext(EventosContext);

    const [page,setPage] = useState(1);
    const [pageSize,setPageSize] = useState(20);

    const [filter,setFilter] = useState({
        title:'',
        resume:'',
        body:''
    });


    const pageChange = (page) => {
        setPage(page);
    }

    const filterChange = (text) => {
            setFilter({
                tipoEvento:text,
                conductorApellido:text,
                conductorDNI:text,
                vehiculoPatente:text,
                vehiculoMarca:text
            })
    }

    useEffect(() => {
      getEventosToPage(page - 1,pageSize,filter);
    }, [page,pageSize,filter]);

  return (
    <><h2 className='p-2'>Listado de eventos</h2>
    <CCard>
    <CCardBody>

      {/* DATATABLE */}
      <div className="position-relative">
      <CDataTable
          items={events}
          fields={[
              {key:'tipoEvento',label:'Tipo de Evento'},
              {key:'fecha',label:'Fecha'},
              {key:'nombre',label:'Conductor Nombre'},
              {key:'apellido',label:'Conductor Apellido', _style: { fontWeigth: "bold" } },
              {key:'dni',label:'Conductor DNI'},
              {key:'patente',label:'Patente'},
              {key:'marca',label:'Marca'},
              {key:'modelo',label:'Modelo'},
          ]}
          tableFilter={{label:"Buscar evento", placeholder:"Ingrese valor de algun campo"}}
          onTableFilterChange={() => { filterChange() }}
          hover
          striped
          responsive
          itemsPerPage={pageSize}
          clickableRows
          onRowClick={(item) => props.history.push(`/evento/${item.id}`)}
          scopedSlots = {{
          'tipoEvento':
              (item)=>(
              <td>
                  <CBadge className="p-2" color={item.tipoEventoId == 3 ? 'danger' : item.tipoEventoId == 2 ? 'info' : 'success' } style={{width:'100%'}}>
                      {item.tipoEvento}
                  </CBadge>
              </td>
              ),
            'patente':
              (item)=>(
              <td>
                  <CBadge className="p-2" color={'success' }>
                      {item.patente}
                  </CBadge>
              </td>
              ),
            'fecha':
              (item)=>(
              item.fecha ?
              <td>
                  <strong>
                      {moment(item.fecha).format('DD/MM/YYYY HH:mm')}
                  </strong>
              </td>
              : 
              <td></td>
              ),
          }}
      />
      
      {/* totalEvents */}
      {totalEvents > 0 && <CPagination
          activePage={page}
          onActivePageChange={pageChange}
          pages={Math.ceil(totalEvents / pageSize)}
          doubleArrows={false} 
          align="center"
      />
      }
      {isEventsLoading && <CElementCover />}
      </div>
    </CCardBody>
  </CCard>
  </>
  )
}

export default EventosList
