import CIcon from '@coreui/icons-react'
import { CButton, CCard, CCardBody, CCardHeader, CCardImg, CCardText, CCardTitle, CCol, CContainer, CElementCover, CRow, CWidgetBrand, CWidgetIcon, CWidgetProgressIcon } from '@coreui/react'
import React, { useContext, useEffect, useState } from 'react'
import EventosContext from 'src/context/eventos/EventosContext';
import moment from 'moment';

const Eventos = ({match}) => {

  const [id,setId] = useState(match.params.id);
  const {getEvento,unselectEvento,eventSelected,loading:isEventsLoading} = useContext(EventosContext);

  useEffect(async () => {
    if(id != null)  {
       await getEvento(id);
    }

    return () => {
      unselectEvento();
    }

}, []);

  return (
    <>
    <h2 className='p-2'>Detalle del evento</h2>
    { eventSelected != null &&
    <CContainer>
    <CRow>

    <CCol md="4" className="py-3">
          <CRow>
              <CCol>
              <CWidgetBrand
          color="info"
          leftHeader="DNI"
          leftFooter={eventSelected.dni}
          rightHeader={eventSelected.apellido}
          rightFooter={eventSelected.nombre}
          style={{  alignContent:'center'}}
          
        >
          <CIcon name="cil-people" height="56"
          className="m-4"  />
     
        </CWidgetBrand>
              </CCol>
          </CRow>
          <CRow>
            <CCol>
            <CCard className='p-1'>
              <CCardImg orientation="top" src={`./images/${eventSelected.id}.jpg`} />  
            </CCard>
            </CCol>
          </CRow>
          
        </CCol>

      <CCol lg="4" className="py-3">
          <CWidgetBrand
            color={eventSelected.tipoEventoId == 3 ? 'danger' : eventSelected.tipoEventoId == 2 ? 'info' : 'success'}
            rightHeader={moment(eventSelected.fecha).format('DD/MM/YYYY')}
            rightFooter="fecha"
            leftHeader={moment(eventSelected.fecha).format('HH:mm')}
            leftFooter="hora"
            style={{  alignContent:'center'}}
          >
            <h2 className="my-5" > {eventSelected.tipoEvento}</h2>
          </CWidgetBrand>
          <CRow>
            <CCol>
              <CWidgetProgressIcon
                header={eventSelected.totalEventos}
                text="Total eventos del conductor"
                color="info"
              >
                <CIcon name="cil-people" height="36"/>
              </CWidgetProgressIcon>
            </CCol>
          </CRow>
        </CCol>
        
      
        <CCol md="4" className="py-3">
          <CWidgetIcon text={eventSelected.modelo} header={eventSelected.marca} color="danger">
            <CIcon name={'cilTruck'} size={'xl'}/>
          </CWidgetIcon>
          <CWidgetIcon text={`PATENTE ${eventSelected.patente}`} header={eventSelected.tipoVehiculoDesc}  color="primary">
            <CIcon name={'cilTruck'} size={'xl'}/>
          </CWidgetIcon>
          <CWidgetIcon text="DIRECCION" header={eventSelected.direccion} color="primary">
            <CIcon name={'cilHome'} size={'xl'}/>
          </CWidgetIcon>
          <CWidgetIcon text="TELEFONO" header={eventSelected.telefono} color="primary">
            <CIcon name={'cilPhone'} size={'xl'}/>
          </CWidgetIcon>
        </CCol>
      </CRow>
    </CContainer>
    }
    {isEventsLoading && <CElementCover />}
    </>
  )
}

export default Eventos
