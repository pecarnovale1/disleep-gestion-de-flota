import AxiosClient from './AxiosClient';

const authToken = async token => {
   
    if(token) {
        AxiosClient.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }
    else
    {
        delete AxiosClient.defaults.headers.common['Authorization'];
    }

}

export default authToken;