import axios from 'axios';

const  AxiosClient =  axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL
});


AxiosClient.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if(error.response) {

        if (error.response.status === 401) {
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
            localStorage.removeItem('user');
            window.location = '/login';
        }
        
        return Promise.reject(error);

    } else {
        return Promise.reject(error);
    }
    
});

export default AxiosClient;