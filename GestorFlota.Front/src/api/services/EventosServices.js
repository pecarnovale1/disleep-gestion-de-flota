import AxiosClient from '../AxiosClient';

const GetStats = () => {
    return AxiosClient.get('evento/stats')
        .then(response => response.data);
}

const GetEventosToPage = (page,pagesize,filter) => {
    return AxiosClient.post('evento/topage',{page,pagesize,filter})
        .then(response => response.data);
}

const GetEvento = (id) => {
    return AxiosClient.get(`evento?id=${id}`)
        .then(response => response.data);
}

export  {
    GetStats,
    GetEventosToPage,
    GetEvento
}