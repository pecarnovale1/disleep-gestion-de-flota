import AxiosClient from '../AxiosClient';

const Login = (user) => {
    return AxiosClient.post('user/login', user)
        .then(response => response.data);
}

const CurrentUser = (user) => {
    return AxiosClient.get('user/currentauthuser')
        .then(response => response.data);
}

export  {
    Login,
    CurrentUser
}