import React, { Suspense } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import {PrivateRoute} from './common';
import AppState from './context/app/AppState';
import AuthState from './context/auth/AuthState';


import './assets/scss/style.scss';
import EventosState from './context/eventos/EventosState';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const Layout = React.lazy(() => import('./containers/Layout'));


// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Register = React.lazy(() => import('./views/pages/register/Register'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));



const App = () => {
    return (
      <AppState>
        <AuthState>
          <EventosState>
          <HashRouter>
              <Suspense fallback={loading}>
                <Switch>
                  <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
                  <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
                  <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                  <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                  <PrivateRoute path="/" name="" component={Layout} />
                </Switch>
              </Suspense>
          </HashRouter>
          </EventosState>
        </AuthState>
      </AppState>
    );
}

export default App;
