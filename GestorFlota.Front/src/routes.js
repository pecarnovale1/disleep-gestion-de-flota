import React from 'react';

const Grid = React.lazy(() => import('./views/grid/Grid'));

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Users = React.lazy(() => import('./views/users/Users'));
const EventosList = React.lazy(() => import('./views/eventos/EventosList'));
const Eventos = React.lazy(() => import('./views/eventos/Eventos'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Estadisticas', component: Dashboard },
  { path: '/eventos', name: 'Eventos', component: EventosList },
  { path: '/evento/:id', name: 'Eventos Detalle', component: Eventos },
  { path: '/grid', exact: true,  name: 'Grilla', component: Grid },
  { path: '/users', exact: true,  name: 'Usuarios', component: Users }
];

export default routes;
