import { CAlert, CButtonClose } from '@coreui/react';
import React, { useContext, useEffect } from 'react';
import AppContext from 'src/context/app/AppContext';

const GlobalNotifications = () => {
    const {error} = useContext(AppContext);

    return <CAlert show={error.visible} color={error.category} className="mt-2" position="static" closeButton={false} >
        
                {error.message}
            </CAlert>
            ;
};

export default GlobalNotifications;