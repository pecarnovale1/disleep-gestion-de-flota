import React, { useContext } from "react";
import { Redirect, Route } from "react-router";
import AuthContext from "../context/auth/AuthContext";

const PrivateRoute = ({ component: Component, ...props }) => {
  const { isAuthenticated } = useContext(AuthContext);
  return (
    <Route
      {...props}
      render={(props) =>
        !isAuthenticated ? <Redirect to="/login" /> : <Component {...props} />
      }
    />
  );
};

export default PrivateRoute;
