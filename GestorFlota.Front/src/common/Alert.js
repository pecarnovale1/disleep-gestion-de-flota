import React, { useEffect, useState } from 'react';
import { CAlert, CButtonClose } from '@coreui/react';

const Alert = (props) => {

    const {visible,category} = props;
    const [showed,setShowed] = useState(false);


    const hideAlert = () => {
        setShowed(false);
    }

    useEffect(() => {
        if(visible)
            setShowed(true);
        else
            setShowed(false);

        const timer = setTimeout(() => {
            setShowed(false);
        }, 5000);
        
        return () => clearTimeout(timer);

    }, [visible]);

    return (
        <CAlert show={showed} color={category}  position="static">
            {props.children}
            <CButtonClose buttonClass="text-danger close" onClick={hideAlert}/>
        </CAlert>
    );
};

export default Alert;